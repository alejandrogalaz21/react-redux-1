import React from 'react'


const TodoItem = ({ id, name, active }) => (
  <li>
    <input
      type="checkbox"
      defaultChecked={active} /> {name}
  </li>
)

const TodoList = (props) => {
  return (
    <div className="todo-list">
      <ul>
        {props.todos.map(todo => <TodoItem key={todo.id} {...todo} />)}
      </ul>
    </div>
  )
}

export default TodoList
