import React, { Component } from 'react';
import './App.css'
import TodoForm from './components/TodoForm'
import TodoList from './components/TodoList'
class App extends Component {

  state = {}

  handleInputChange = event => {
    const target = event.target
    const value = target.type === 'checkbox' ? target.checked : target.value
    const name = target.name
    this.setState({ [name]: value })
  }

  render() {
    return (
      <div className="App">
        <div className="todo-app">

          <TodoForm />

          <TodoList todos={this.props.todos} />

        </div>
      </div>
    )
  }
}

export default App;
